%define last_entry 0

%macro colon 2
    %ifid %2
        %2: dq last_entry
        %define last_entry %2
    %else
        %error "second argument not id"
    %endif
    %ifstr %1
        db %1, 0
    %else
        %error "first argument not string"
    %endif
%endmacro
