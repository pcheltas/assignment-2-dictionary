%include "words.inc"
%include "dict.inc"
%include "lib.inc"

%define buffer_size 256
%define OFFSET 8
%define COMPENSATION 1

section .bss
buffer: resb buffer_size

section .rodata
overflow_msg: db "String is too long", 0
not_found_msg: db "Word was not found", 0

section .text
global _start

_start:
	mov rdi, buffer
	mov rsi, buffer_size
	call read_word
	test rax, rax
	jz .overflow
	
	mov rdi, buffer
	mov rsi, last_entry 
	call find_word
	test rax, rax
	jz .not_found
	
	lea rdi, [rax+OFFSET]
	call string_length
	lea rdi, [rdi+rax+COMPENSATION]
	call print_string
	jmp exit

.overflow:
	mov rdi, overflow_msg
	call string_length
	mov rdx, rax				
	mov rax, 1				
	mov rdi, 2				
	mov rsi, overflow_msg
	syscall
	jmp exit

.not_found:
	mov rdi, not_found_msg
	call print_string
	jmp exit
