import subprocess


command = "./executable_program"


input_strings = ["key1", "key2", "key3", "udvnhowdvn", "d"*260]

expected_outputs = ["I am", "so fucking", "tired", "Word was not found", ""]

expected_errors = ["", "", "", "", "String is too long"]


for input_string, expected_output, expected_error in zip(input_strings, expected_outputs, expected_errors):
    
    process = subprocess.Popen(command, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    stdout, stderr = process.communicate(input_string.encode())

    recieved_stdout = stdout.decode()
    recieved_stderr = stderr.decode()
    
    if recieved_stdout == expected_output:
        print(f"Right stdout for {input_string}: {expected_output}")
    else:
        print(f"Wrong stdout for {input_string}: {recieved_stdout}, expected {expected_output}")
        
    if recieved_stderr == expected_error:
        print(f"Right stderr for {input_string}: {expected_error}")
    else:
        print(f"Wrong stderr for {input_string}: {recieved_stderr}, expected {expected_error}")    
