
TARGET = executable_program
ASSEMBLER = nasm
ASFLAGS = -f elf64
LD = ld
LDFLAGS = -o $(TARGET)
SOURCES = dict.asm lib.asm main.asm
OBJECTS = $(SOURCES:.asm=.o)
PYTHON = python3
TEST_SCRIPT = test.py

.PHONY: all test clean

all: $(TARGET)

%.o: %.asm
	$(ASSEMBLER) $(ASFLAGS) $< -o $@

$(TARGET): $(OBJECTS)
	$(LD) $(LDFLAGS) $(OBJECTS)

test:
	$(PYTHON) $(TEST_SCRIPT)

clean:
	$(RM) $(OBJECTS) $(TARGET)
