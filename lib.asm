global exit
global string_length
global print_string
global print_newline
global print_char
global print_int
global print_uint
global string_equals
global read_char
global read_word
global parse_int
global parse_uint
global string_copy




section .text
 
 %define SYS_EXIT 60 
 %define SYS_WRITE 1
 %define SYS_READ 0
 %define STDOUT 1
 %define STDIN 0
 
; Принимает код возврата и завершает текущий процесс
exit: 
    	mov rax, SYS_EXIT
    	syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    	xor rax, rax
    	.loop:
    	cmp byte[rdi + rax], 0			;ret if n-char is null
    	je .end
    	inc rax					;inc counter
    	jmp .loop
    	.end:
    	ret
    
; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    	push rdi
    	call string_length
    	mov rdx, rax				;number of bytes to write
    	pop rsi					;address of the string beginning
    	mov  rax, SYS_WRITE				;write syscall
    	mov  rdi, STDOUT				;write to stdout
    	syscall
    	ret
    


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
 	mov rdi, 0xA				;then executes print_char with '/n' as arg

; Принимает код символа и выводит его в stdout
print_char:
	push di				;char to stack
	mov rax, SYS_WRITE				;write syscall
	mov rsi, rsp				;read char from stack
	mov rdi, STDOUT				;write to stdout
	mov rdx, 1				;1 byte to write
	syscall
	add rsp, 2
    	ret


; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
	mov rax, rdi				
	test rdi, rdi				
	jnl print_uint				;if >0 execute print_uint
	mov rdi, '-'
	push rax
	call print_char				;print '-'
	pop rax
	neg rax					;neg num
	mov rdi, rax				;load num as arg (with no '-'), execute print_uint


; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
	mov rax, rdi
	mov r8, rsp				;copy rsp to restore at the end
	push 0					;0 to stack for null-terminated string
	mov r9, 10				;divider 
	.loop:
		xor rdx, rdx
		div r9
		add rdx, '0'			;convert remainder into ASCII
		dec rsp
		mov byte[rsp], dl		;write num char to stack
		test rax, rax			
		jnz .loop	
	mov rdi, rsp				;load arg for print_string
	push r8					;save original rsp
	call print_string
	pop rsp					;restore rsp
    	ret
    	

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    	xor rax, rax				;clear counter
  	.loop:
  		xor rdx, rdx			
  		mov dl, [rsi+rax]
        	cmp dl, [rdi+rax]		;compare chars
  		jne .false			;if different false
  		test dl, dl			;if same and null - end of strings, true
  		je .true			
  		inc rax
  		jmp .loop	
  	.false:
  		xor rax, rax
  		ret
  	.true:
  		mov rax, 1	
  		ret
    	

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	push 0					;rsp+1, rsp+1 stores 0 if eof
    	mov rax, SYS_READ
    	mov rdi, STDIN
    	mov rsi, rsp
    	mov rdx, 1
	syscall
	mov rax, [rsp]				;move char as result
	pop r8					;restore rsp
    	ret
	

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
	push r12				;save calee-saved registers
	push r13
	push r14
    	mov r12, rdi				;byffer address
    	mov r13, rsi				;buffer size
    	xor r14, r14				;clear counter
    	.read_char:
    		call read_char
    		test rax, rax			;if end - finish
 		je .true
 		cmp r14, r13			;if word longer then buffer - false
 		jge .false
    		cmp rax, 0x20			;check for space
    		je .first_char
    		cmp rax, 0x9			;tab
 		je .first_char
 		cmp rax, '\n'			;\n
 		je .first_char	
 		mov [r12+r14], rax		;save char n to buffer
 		inc r14				;inc counter
 		jmp .read_char
 	.first_char:				;if space/tab/'\n' in the beginnig skip
 		test r14, r14			;else finish
 		je .read_char
 		jne .true
 	.true:
 		mov byte [r12+r14+1], 0		;add 0 for null-terminated
 		mov rax, r12
 		mov rdx, r14
 		jmp .finish
 	.false:
 		xor rax, rax
 		xor rdx, rdx
 	.finish:
 		pop r14				;restore calee-saved registers
 		pop r13
 		pop r12
 		ret
 			
 		
 		


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
	mov r8b, byte [rdi]			;read first char
	cmp r8b, '-'				;if not '-' parse as uint
	jne parse_uint
	cmp r8b, '+'
	je parse_uint
	inc rdi					;addres of first char after '-'
	call parse_uint
	neg rax					;neg uint parsing
	inc rdx					;inc string length for '-' symbol
	ret
	
    
; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax        			;clear result register
    xor rdx, rdx        			;clear string length
    .loop:
        mov r8b, byte [rdi+rdx]  		;get first char
        cmp r8b, '0'				;if not number finish
        jb .done               
        cmp r8b, '9'
        ja .done               
	inc rdx 
        imul rax, rax, 10      			;multiply current value by 10
        sub r8b, '0'           			;convert ASCII into num
        add rax, r8           			;add to current value            
        jmp .loop
    .done:
        ret    
    
    

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax				;string length
    .loop:
    	cmp rax, rdx				;false if string longer then buffer
    	jge .false
    	mov r8b, byte [rdi+rax]
    	mov byte [rsi+rax], r8b			;write string symbol to buffer
    	cmp byte [rsi+rax], 0			;check if end
    	inc rax					;inc string length
    	jne .loop 
    	ret
    .false:
    	xor rax, rax
    	ret
    	

   	
