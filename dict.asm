%include "lib.inc"
%define OFFSET 8
global find_word

section .text

find_word:
	push r12 
	push r13
	mov r12, rdi				;rdi stores pointer of null-terminated string
	mov r13, rsi				;rsi stores pointer of beginning of dictionary
	.loop:
		test r13, r13
		je .not_found
		mov rdi, r12
		lea rsi, [r13 + OFFSET]
		call string_equals
		test rax, rax
		jne .found
		mov r13, [r13]
		jmp .loop	
		
	.not_found:
		xor rax, rax
		jmp .restore_and_ret
	.found:
		mov rax, r13
		jmp .restore_and_ret
		
	.restore_and_ret:
		pop r13
		pop r12
		ret
